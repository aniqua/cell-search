#!/bin/sh
#
# Install Falcon
#

exit_on_error() {
    exit_code=$1
    last_command=${@:2}
    if [ $exit_code -ne 0 ]; then
        >&2 echo "\"${last_command}\" command failed with exit code ${exit_code}."
        exit $exit_code
    fi
}

sudo apt-get -y install --no-install-recommends build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
if [ $? -ne 0 ]; then
    echo 'apt-get install srsRAN dependencies failed'
    exit 1
fi


# enable !! command completion
set -o history -o histexpand

cd /local/tools
exit_on_error $? !!

git clone --recursive https://gitlab.flux.utah.edu/aniqua/srsran.git
exit_on_error $? !!

cd srsran
exit_on_error $? !!

mkdir build
exit_on_error $? !!

cd build
exit_on_error $? !!

cmake ../
exit_on_error $? !!

make
exit_on_error $? !!

make test
exit_on_error $? !!

make install
exit_on_error $? !!

srsran_install_configs.sh user

exit 0